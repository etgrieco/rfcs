---
layout: single
permalink: /
---

Welcome to the Democratic Socialists of America National Tech Committee Request
For Comment system! Here you can find a list of projects that the national tech
committee has proposed, the rationale and reasoning behind those projects. In
addition you can propose projects yourself as well that you would like the
National Tech Committee to work on. We're still figuring out how to make all
this work and it's all very exciting. So, please, join in and help us build
democratic socialism!
