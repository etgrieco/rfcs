---
title: "Template RFC" 
author_name : "Karl & Rosa"
status: "Submitted"
last_update:   2017-09-24 21:57:46 -0400
layout: rfc
---
Test

This is a template RFC that you can use to create and submit your own requests
for comment.

# Summary
[summary]: #summary

One paragraph explanation of the feature.

# Motivation
[motivation]: #motivation

Why are we doing this? What use cases does it support? What is the expected
outcome? Imagine that you are reading this RFC several years from now, without
the context of the present situation close at hand.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

Explain the proposal as if it were already a policy or technical solution
adopted by the Tech Committee and you were teaching it to a new member or to a
member of the national staff or political committee:

- Introduce new concepts clearly and explain their intended effects.
- Provide concrete examples of how this RFC will benefit DSA Members, the Tech Committee, the NPC, etc.
- If applicable, provide an explanation of any deprecated features, tools, or policies, and provide migration guidance.
- Provide a summary of what this RFC is not intended to cover and the rationale for that.

For a technically-oriented RFC, give examples of its concrete impact. For a
policy RFC, this section should provide an example-driven introduction to the
policy, and explain its impact in concrete terms.

# Technical explanation
[reference-level-explanation]: #reference-level-explanation

This is the technical portion of the RFC. Explain the design in sufficient detail that:

- Its interaction with other parts of DSA's digital infrastructure is clear.
- It is reasonably clear how the project would be implemented.
- Corner cases are dissected by example.

The section should return to the examples given in the previous section, and
explain more fully how the detailed proposal makes those examples work.

# Prior Work
[prior-work]: #prior-work

If this RFC builds on previous work done in the same area, or references or
supercedes a prior RFC, note that here.

# Cost
[cost]: #cost

Explain any expected costs required for the implementation of this RFC. This
should include staff time or dollar figure for goods and services required.

# Drawbacks
[drawbacks]: #drawbacks

Why should we *not* do this?

# Alternatives
[alternatives]: #alternatives

- Why is this policy or implementation the best in the space of possible alternatives?
- What other options have been considered and what is the rationale for not choosing them?
- What is the impact of not doing this?

# Unresolved questions
[unresolved]: #unresolved-questions

- What parts of this RFC do you expect to resolve through the RFC process before this gets merged?
- What parts of this RFC do you expect to resolve through discussion before stabilization or publication?
- What related issues do you consider out of scope for this RFC that could be
  addressed in the future independently of the solution that comes out of this
  RFC?
